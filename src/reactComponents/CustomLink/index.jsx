import { Component } from "react"

import { Route, Link, withRouter } from "react-router-dom"
import Discover from "./Discover"
import Mine from "./Mine"
import CusLink from "./CusLink"
@withRouter
class CustomLink extends Component {
    handleJump = () => {
        //push 跳转页面并且添加一个历史记录
        console.log(this.props)
        //方法1:
        // this.props.history.push("/discover")
        //方法2:
        //react路由可以传递任意的字段
        this.props.history.push({ pathname: "/discover?name=阿拉蕾", query: { name: '李雷' }, abc: { msg: '为什么大家都这么困' } })
        //replace 跳转页面,替换上一个历史记录
        //goBack 返回上一个历史记录
        //goForward 前进下一个历史记录
        //go(number)  跳转到某个历史记录
    }
    render() {
        return (

            <fieldset>
                <legend>自定义link</legend>
                <CusLink to="/discover">发现</CusLink>
                <br />
                <CusLink to='/mine'>个人中心</CusLink>
                <br />
                <Link to="/discover?name=李雷">发现传递query</Link>

                <Route path="/discover" component={Discover} />
                <Route path='/mine' component={Mine} />

                {/* children:func | React.Component 不需要匹配路径,直接会展示组件 Route匹配成功 组件的props有值,如果匹配不成功那么match就是null*/}

                {/* <Route path="/discover" children={Discover} />
                <Route path='/mine' children={Mine} /> */}
                <button onClick={this.handleJump}>一个朴实无华的按钮</button>
            </fieldset>

        )
    }

}


export default CustomLink
