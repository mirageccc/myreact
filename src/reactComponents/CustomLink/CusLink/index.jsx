import { Link, Route } from "react-router-dom"

const CusLink = props => {
    // console.log(props)
    let { to, children } = props;

    const fn = (props) => {
        let { match } = props;
        // console.log('浏览器当前地址:'+props.location.pathname)
        // console.log(match)

        return <Link to={to}>{
            match ? '☎' + children : children
        }</Link>
    }

    return <Route path={props.to} children={fn} />


}

export default CusLink