import React from "react"

import store from "../../store"
// console.log(store.getState())
//引入类型
import * as chatActionTypes from "../../store/actions/chat"
//引入actionCreator
import act from "../../store/actions/actionCreator"
class Wechat extends React.Component {
    state = {
        val: ""
    }
    handleChange = (e) => {
        this.setState({
            val: e.target.value
        })
    }
    //点击添加聊天信息到redux
    add = () => {
        store.dispatch(act(chatActionTypes.ADD_CHAT, this.state.val))
    }
    render() {
        let { chatLog, isVip, name } = store.getState() //获取状态

        let lis = chatLog.map((item, index) => {
            return <li key={index}>{item}</li>
        })
        return (
            <fieldset>
                <legend>聊天</legend>
                <h1>姓名:{name}</h1>
                <h3>{isVip ? 'vip100' : <button onClick={() => {
                    store.dispatch(act(chatActionTypes.CHANGE_VIP))
                }}>点击充值</button>}</h3>
                <input type="text" value={this.state.val} onChange={this.handleChange} />
                <button onClick={this.add}>点击聊天</button>
                <ul>
                    {lis}
                </ul>
            </fieldset>
        )
    }
}

export default Wechat