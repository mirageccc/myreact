import React from "react"

import store from "../../store"
// import * as actions from "../../store/actions/counter"
import * as actionTypes from "../../store/actions/counter"
import actionCreator from "../../store/actions/actionCreator"
//state就是状态 包含了store中的所有数据

//变更状态的唯一方式就是通过dispatch派发action
//dispatch 派发 是派发action的唯一方式,这个函数里面必须接受一个纯对象
console.log(store.getState()) //99

//subscribe 订阅状态的变化,传入一个函数,状态变化就回调传入的函数

store.subscribe(() => {
    console.log(store.getState())
})

class Counter extends React.Component {
    handleIncrease = () => {
        // console.log(actions)
        //自增
        // store.dispatch(actions.INCREASE_COUNT)

        //使用creator
        store.dispatch(actionCreator(actionTypes.INCREASE_COUNT, 20))
    }
    handleDecrease = () => {
        //自减
        // store.dispatch(actions.DECREASE_COUNT)

        //使用creator

        store.dispatch(actionCreator(actionTypes.DECREASE_COUNT, 10))
    }
    render() {
        return (
            <fieldset>
                <legend>计数器</legend>
                <button onClick={this.handleDecrease}>-</button>
                &nbsp;
                <b>{store.getState()}</b>
                &nbsp;
                <button onClick={this.handleIncrease}>+</button>
            </fieldset>
        )
    }
}

export default Counter