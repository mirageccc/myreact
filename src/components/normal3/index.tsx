import HOC from "../HOC/index3"

const Normal3 = (props:any) =>{
    console.log(props)
    return(
        <fieldset>
            <legend>一个朴实无华的Normal3</legend>
            <h1 style={{color:props.color,backgroundColor:props.backgroundColor}}>Normal3</h1>
            
        </fieldset>
    )
}

let cb = function(state:any){
    console.log(state)
    return {
       color:"blue"
    }
}

let theme = {
    color : "red",
    backgroundColor : "deepskyblue",
    opacity :.8
}


let newNormal3 = HOC(theme,cb)(Normal3)

export default newNormal3