import React, {FC} from "react"
type p ={
  children:Array<React.ReactElement>
  [T:string]:React.ReactElement | Array<React.ReactElement>
}
let Composition:FC<p>

//组合相当于vue里面的插槽 比vue更灵活
Composition = function Composition(props){
    console.log(props);
    
    return (
        <fieldset>
            <legend>组合</legend>
            <h1>一些内容</h1>
            <div className="header">
                {props.header}
            </div>
            <div className="main">
                {props.main}
            </div>
            <div className="footer">
                {props.footer}
            </div>
            
        </fieldset>
    )
}
export default Composition