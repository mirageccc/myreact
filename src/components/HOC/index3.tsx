import React from 'react'
//声明HOC的类型

//HOC最终接受一个组件返回一个组件 React.ComponentType<限制props类型>

//这里使用type 可以描述单一元素的类型  interface是描述对象数据类型的
type HC<t,c,p> = (themeObj:t,cb:c)=>(Component:React.ComponentType<p>)=>React.ComponentType<p>

type theme = {//约束主题的类型
    color: string
    backgroundColor: string
    opacity: number
}

//约束回调函数的类型

type s<T> = {
    // title:string
    [k in keyof T]?:T[k]
}

type cb = (state:s<theme>)=> s<theme>  

//约束props

type Iprops ={
    name: string
}
//这个高阶组件用于渲染主题
const HOC3:HC<theme,cb,Iprops> = (themeObj,cb)=>(Component) =>props=>{

    let state ={
        title:"隐藏属性",
        ...themeObj
    }
    if(cb  && typeof cb ==="function"){
        let res = cb(state)
        themeObj.color = res.color!
    }

   return <Component  {...themeObj} {...props}/>
}

export default HOC3