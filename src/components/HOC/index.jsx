//高阶组件

//作用:复用react中的组件逻辑

//概念:高阶组件是一个函数(组件)，接收一个组件，处理之后返回一个新的组件

//高阶组件的属性代理模式
const WithTheme = (Component) =>{
    let theme = {
        color: "hotpink",
        backgroundColor: "deepskyblue",
        fontsize:"60px",
        border:"1px double #000"
    }
    let state={
        title:"隐藏属性"
    }

    return function(){
        return<Component {...theme}/>
    }
}

export default WithTheme