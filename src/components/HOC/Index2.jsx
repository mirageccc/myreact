//高阶组件

//作用:复用react中的组件逻辑

//概念:高阶组件是一个函数(组件)，接收一个组件，处理之后返回一个新的组件

//高阶组件的属性代理模式
const WithTheme2 = function (cb){
    return function (Component) {
        let theme = {
            color: "hotpink",
            backgroundColor: "deepskyblue",
            fontsize:"60px",
            border:"1px double #000"
        }


        let state ={
            title: "隐藏属性"
        }
        // console.log(cb);
        //回传state隐藏属性

        if(cb&&typeof cb =="function"){
           let res =  cb(state,theme)
           //接收getState函数回传的结果
           if(res){
            for(let k in res){
                theme[k]=res[k]
              }
           }
        }

        return function(props){
            return<Component {...theme}/>
        }
    }
}

export default WithTheme2