import HOC from "../HOC/index"

const Normal1 = props =>{
    return(
        <fieldset>
            <legend>一个朴实无华的Normal1</legend>
            <h1 style={{color:props.color,backgroundColor:props.backgroundColor}}>Normal1</h1>
            
        </fieldset>
    )
}


export default HOC(Normal1)