import {Component} from "react"

import {createPortal} from "react-dom"
//portal是react 的高级api之一 可以将组件的内容传送到任意的dom元素里面
class PortalCom extends Component<any,any> {
    modalRoot:Element
    constructor(props:any) {
        super(props)
        this.modalRoot = document.createElement('div')
        this.modalRoot.id = 'modal-root'
    }
    componentDidMount(){
        document.body.appendChild(this.modalRoot)
    }
    render() {
        let el:React.ReactElement=  (<fieldset>
        <legend>123</legend>
        <h1>123</h1>
    </fieldset>)
    //创建了一个传送门，然后将el 传送倒了modal-root里面
        let portal = createPortal(el,this.modalRoot)
        return portal 
}
}
export default PortalCom