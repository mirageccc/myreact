import React, { Component, ReactNode } from "react"
import { createPortal } from "react-dom"
import { createImportSpecifier } from "typescript"
import "./index.css"

interface Iprops {
    visible?: boolean //控制模态框显示和隐藏的
    onOk?: () => void //点击确定的方法
    onCancel?: () => void //点击取消的方法
    children?: Array<React.ReactElement> | React.ReactNode
    title?: string
}

interface Istate {
    isShow: boolean;
    isClickX: boolean;
}

class Modal extends Component<Iprops, Istate> {
    modalRoot: Element
    constructor(props: Iprops) {
        super(props)
        this.state = {
            isShow: false,
            isClickX: false //是否点击了x按钮
        }
        this.modalRoot = document.createElement('div')
        this.modalRoot.id = "modal-root"
    }
    componentDidMount() {
        //组件挂载的时候添加到页面上
        document.body.appendChild(this.modalRoot)
    }
    componentWillUnmount() {
        //判断存不存在modalRoot这个dom元素
        let modalRoot = document.getElementById('modal-root')
        if (modalRoot) {
            document.body.removeChild(modalRoot)
        }
    }
    //需要通过传递下来的props来衍生一个新的state
    static getDerivedStateFromProps(newProps: Iprops, newState: Istate) {
        // console.log('newProps===', newProps)
        //如果点击的是x,就不要从props来派发新的状态而是直接将state的isShow变成false
        //判断是否点击了x
        // console.log(newState.isClickX)
        if (newState.isClickX) {
            //点了x
            return {
                isShow: false,
                isClickX: false //充值x的点击状态false
            }
        } else {
            return {
                isShow: newProps.visible
            }
        }
    }
    handleOk = () => {
        if (this.props.onOk) {
            this.props.onOk!()
        }
    }
    handleCancel = () => {
        if (this.props.onCancel) {
            this.props.onCancel!()
        }
    }
    //点击x按钮关闭模态框
    handleClickX = () => {
        this.setState({
            isClickX: true
        })
    }
    render(): ReactNode {
        let { isShow } = this.state;//解构state中的方法
        let { children, title } = this.props;
        let modalEl = (
            <div className="modal-container">
                <div className="modal-mask">
                    <div className="modal-content">
                        <button className="close" onClick={this.handleClickX}>×</button>
                        <div className="header">
                            {title ? title : '提示'}
                        </div>
                        <div className="body">
                            {children ? children : '模态框'}
                        </div>
                        <div className="footer">
                            <button onClick={this.handleCancel} className="cancel">取消</button>
                            <button onClick={this.handleOk} className="confirm">确定</button>
                        </div>
                    </div>
                </div>
            </div>
        )
        //将结构的传送到指定dom的位置
        let portal = createPortal(modalEl, this.modalRoot)
        return isShow ? portal : null
    }
}

export default Modal