import {FC,createContext} from "react"
//comtext
//掌握两个api
//Provider 提供
//Consumer 消费


//context的泛型用于限制defaultValue

//context的默认值defaultValue是在没有provider的时候才会启动
interface c{
    color: string;
    [T: string]:string | number;
}
let {Provider,Consumer} = createContext<c>({color:"red"})
type p ={



}
const D:FC<p> = ()=>{
    return (
        <fieldset>
            <legend>D</legend>
            <Consumer>
            {
              props =>{
                console.log(props);
                return <h1 style={{color:props.color}}>组件D</h1>
                
              }
            }
            </Consumer>
        </fieldset>
    )
} 
const C:FC<p> = ()=>{
    return (
        <fieldset>
            <legend>C</legend>
            <h1>组件C</h1>
            <D/>
        </fieldset>
    )
} 
const B:FC<p> = ()=>{
    return (
        <fieldset>
            <legend>B</legend>
            <h1>组件B</h1>
            <C/>
        </fieldset>
    )
} 
const A:FC<p> = ()=>{
    let gift = "react从入门到精通"
    let color = "green"
    return (
        <fieldset>
            <legend>A</legend>
            <h1>组件A</h1>
            <Provider value={{gift,color}}>
            <B/>
            </Provider>
        </fieldset>
    )
} 




export default A