import HOC from "../HOC"
import HOC2 from "../HOC/Index2.jsx"
import {Component} from "react"
//高阶组件有个公共的隐藏属性 需要我们在使用组件的时候传递方法才能够获取
@HOC2()
class Normal2 extends Component{
    render(){
        return(
            <fieldset>
                <legend>一个朴实无华的Normal2</legend>
                <h1 style={{color:this.props.color}}>Normal2</h1>
                <button>点击</button>
                <input type="text"  style={{backgroundColor:this.props.backgroundColor}}/>
            </fieldset>
        )
    }
    
}

//getState 获取高阶组件回传的隐藏属性
const getState = (state,theme)=>{
    console.log(state,theme)

    return {
        color:"red",
        backgroundColor: "green"
    }
}

export default Normal2