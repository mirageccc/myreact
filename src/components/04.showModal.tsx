//展示Modal

import { Component,ReactNode } from "react"
import Modal from "./04.modal/index"

type s = {
    visible: boolean
}
class showModal extends Component<any,s> {
    constructor(props:any){
        super(props)
        this.state={
            visible: false
        }
    }
    handleClick = ()=>{
        this.setState({
            visible: true
        })
    }
    //处理点击确定的按钮
    handleOk=() =>{
       console.log("点击了确定");
       this.setState({visible: false})
    }
    //处理点击取消的按钮
    handleCancel=() =>{
        console.log("点击了取消");
        this.setState({visible: false})
    }
    //处理点击确定的

    render():ReactNode{
        return (
            <fieldset>
                <legend>封装组件</legend>
                <button onClick={this.handleClick}>点击</button>
                <Modal title='⚠警告' visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}></Modal>
            </fieldset>
        )
    }
}
export default showModal