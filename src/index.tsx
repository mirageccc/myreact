import { Fragment } from "react"
import ReactDOM from "react-dom/client"
import App from "./App"
import store from "./storeTs"
import * as type from "./types/redux"
import actionCreator from "./storeTs/actions/actionCreator"
// console.log(store);


//thunk的作用 可以让dispatch传递函数
store.dispatch(actionCreator({ type: 'GET_PRODUCT', payload: 12 }))

let root = ReactDOM.createRoot(document.getElementById('root')!)
root.render(
    <Fragment>
        <App />
    </Fragment>
)