import { combineReducers } from "redux"
import Cart from "./Cart"
import Product from "./Product"

let rootReducer = combineReducers({
    Product,
    Cart
})

export default rootReducer