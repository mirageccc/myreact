
import { carts } from "../../types/redux"

type rState = Array<any>

interface rAction {
    type: string;
    [T: string]: any
}

let initialState: rState = ['carts']


const cartReducer: carts<rState, any> = (state = initialState, action) => {
    return [...state]
}

export default cartReducer