import { carts } from "../../types/redux"

type c = Array<any>

type a = {
    type: String;
    [T: string]: any
}

let initialState: c = ['product']

//reducer 只能进行同步逻辑

const productReducer: carts<c, any> = (state = initialState, action) => {
    let { type, payload } = action;
    console.log(payload)
    switch (type) {
        case "GET_PRODUCT":
            return [...payload]
        default:
            return [...state]
    }

}


export default productReducer