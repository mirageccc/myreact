const actionCreator = arg => {
    // console.log(arg)
    return (dispatch, getState, api) => {
        //异步操作,并且要提交action
        // console.log(api) //请求的方法
        console.log(dispatch)
        api.then(res => {
            console.log(res)
            //派发一个对象给redux
            dispatch({ type: arg.type, payload: res.data })
        })
    }
}

export default actionCreator