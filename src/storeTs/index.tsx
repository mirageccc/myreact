import { configureStore } from "@reduxjs/toolkit"

import rootReducer from "./reducers"

//一些异步的逻辑或者一些额外的日志操作都需要中间件,redux提供了我们添加中间件的机会

//所有的中间件都是一个函数,接收一个store作为参数,store中有getState() dispatch

// import testMiddleware from "../middlewares/test"

//常用的中间件有哪些? redux-logger日志 redux-thunk处理异步操作 redux-promise 处理异步操作

// import logger from "redux-logger"

import { getProduct } from "../api"

let rootStore = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware({
            thunk: {
                extraArgument: getProduct
            }
        })

})

export default rootStore