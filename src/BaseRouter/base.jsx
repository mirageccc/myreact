import { Component } from "react"
import Discover from "./Discover"
import Friends from "./Friends"
import Mine from "./Mine"
import "./base.css"
//路径切换 NavLink

//匹配路由 Route

//路由的容器 BrowserRouter HashRouter


//NavLink 1.activeClassName 2.activeStyle 3.isActive:fun

import { HashRouter as Router, NavLink, Route, Switch } from "react-router-dom"
import Page404 from "./Page404"

class BaseRouter extends Component {
    state = {
        s: "朋友"
    }
    render() {
        return (
            <Router>
                <fieldset>
                    <legend>基础路由</legend>
                    <NavLink to={{ pathname: "/" }}>个人中心</NavLink>
                    &nbsp;
                    |
                    <NavLink to={{ pathname: "/d" }}>发现</NavLink>
                    &nbsp;
                    |
                    &nbsp;
                    <NavLink to="/friends/hmm" isActive={(match) => {
                        // console.log(match)

                        // console.log('friends')
                        //激活的时候match是一个对象
                        //没激活的时候match是null
                        //不能调用setState 因为会死循环09
                        if (match) {
                            console.log('路径被激活了')
                        } else {
                            console.log(' 路径没有激活')
                        }

                    }}>{this.state.s}</NavLink>

                    <Switch>
                        {/* 路由匹配顺序默认是从上到下,使用Switch让路由一次只能匹配一个 */}
                        <Route path="/d" component={Discover} />
                        {/* 可以限制动态路由传递的参数是哪些 */}
                        <Route path="/friends/:name(lilei|hmm|cjh)" component={Friends} />
                        {/* exact 严格匹配 */}
                        <Route path="/" exact component={Mine} />
                        {/* 配置404页面 */}
                        <Route component={Page404} />
                    </Switch>
                </fieldset>
            </Router>
        )
    }
}

export default BaseRouter