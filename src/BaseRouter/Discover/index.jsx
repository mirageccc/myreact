//哪里配路由 哪里就写route
import { Route, Link,Redirect} from "react-router-dom"
import Djradio from "./Djradio"
import Toplist from "./Toplist"
const Discover = props => {
    console.log(props)
    return (
        <fieldset>
            <legend>发现</legend>
            <h1>发现首页</h1>
            <ul>
                <li>
                    <Link to={`${props.match.path}/djradio`}>主播电台</Link>
                </li>
                <li>
                    <Link to={`${props.match.path}/toplist`}>排行榜</Link>
                </li>
            </ul>
            <Redirect from={`${props.match.path}`} to={`${props.match.path}/djradio`} />
            <Route path={`${props.match.path}/toplist`} component={Toplist} />

            <Route path={`${props.match.path}/djradio`} component={Djradio} />
        </fieldset>
    )
}

export default Discover