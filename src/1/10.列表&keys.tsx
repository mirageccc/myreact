//列表渲染
import React, { ReactNode } from 'react';

interface Ic {
    state: {
        arr: Array<string>
    }
}

type Iprops = {}


class List extends React.Component<Iprops> implements Ic {
    state = {
        arr: ['吃饭', '吃饭2', '吃饭3']
    }
    render(): React.ReactNode {
        let { arr } = this.state;
        //react会自动展开数组里面的元素
        // let arr2: React.ReactNode = [
        //     <li>1</li>,
        //     <li>2</li>,
        //     <li>3</li>
        // ]
        //核心思路:根据数据 将react节点穿件出来,返回到一个数组就行了
        //key的作用:提升效率 区分元素的唯一性
        let lis: React.ReactNode = arr.map((item: React.ReactNode, index: number): React.ReactNode => {
            return (
                <li key={index}>{item}</li>
            )
        })
        return (
            <fieldset>
                <legend>列表</legend>
                <ul>
                    {lis}
                </ul>
            </fieldset>
        )
    }
}


export default List
