import React from "react"

type Iprops = {
    age: number
}

interface Ic {
    state: {
        name: string
    }
}

class SuppressRenderCom extends React.Component<Iprops> implements Ic {
    state = {
        name: "李雷"
    }
    render(): React.ReactNode {
        // 阻止组件渲染只需要return null就可以了
        // 阻止组件渲染 组件的生命周期钩子函数依旧会执行
        let { age } = this.props;
        if (age > 18) {
            return (
                <h1>组件渲染了</h1>
            )
        } else {
            return null
        }

    }
}

export default SuppressRenderCom