
import React from "react"
import "./03.css"

interface Iprops {
    name?: string
}

interface Istate {
    name: string;
    isVip: boolean
}

interface Ic { //定义类里面的方法
    handleCopy(e?: any): void
    handleCopyEvent(): void
    handlePasteEvent(): void
}
//剪贴板事件

//onPaste 粘贴事件
//onCut 剪切事件
//onCopy 拷贝事件

//和剪贴板操作相关的方法

//window.getSelection().toString() 获取选中内容

//navigator.clipboard.readText().then(res=>res) 读取剪贴板内容

//navigator.clipboard.writeText('content) 写入内容到剪贴板

class ClipBoardEdit extends React.Component<Iprops, Istate> {
    state = {
        name: '李雷',
        msg: '二愣子睁大着双眼，直直望着茅草和烂泥糊成的黑屋顶，身上盖着的旧棉被，已呈深黄色，看不出原来的本来面目，还若有若无的散发着淡淡的霉味。',
        isVip: false
    }
    handleCopy = (e?:Event) => {
        console.log(e)
    }
    //剪贴板的读取操作
    handleCopyEvent = () => {
        //不是vip就让他充值
        if (!this.state.isVip) {
            alert('请充值vip以获得复制权限');
            return
        }
        // 获取选中的文本
        let selectText: string = window.getSelection()!.toString();
        // console.log(selectText);
        //写入到剪贴板
        navigator.clipboard.writeText(selectText)
    }
    //剪贴板的写入操作
    handlePasteEvent = () => {
        if (!this.state.isVip) {
            alert('请充值vip以获得复制权限');
            return
        }
        //读取剪贴板内容
        navigator.clipboard.readText()
            .then(res => {
                // console.log(res)
                this.setState({ name: res })
            })
    }
    handleCharge = () => {
        this.setState({ isVip: true })
    }
    //钩子函数
    componentDidMount() {
        // 把组合键的事件阻止
        document.onkeydown = function (e) {
            e.preventDefault()
        }
    }
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>剪贴板的操作</legend>
                <div className="dv" contentEditable={true} suppressContentEditableWarning={true}>
                    {this.state.msg}
                </div>
                <br />
                <div className="dv" contentEditable={true} suppressContentEditableWarning={true}>
                    {this.state.name}
                </div>
                <button onClick={this.handleCopyEvent.bind}>复制</button>
                <button onClick={this.handlePasteEvent}>粘贴</button>
                <button onClick={this.handleCharge}>充值vip获取复制权限</button>
            </fieldset>
        )
    }
}

export default ClipBoardEdit