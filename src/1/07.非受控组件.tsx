import React from "react"

//非受控组件 就是将控制权交换给dom本身 就是通过ref来实现的

// ref ref作用在元素上 那么就能获取dom元素的引用

// 如果作用在子组件上面 那么能拿到子组件实例的引用


interface n {
    iptRef: React.RefObject<HTMLInputElement>
}

class NotControl extends React.Component<any, any> implements n {
    iptRef
    constructor(props: any) {
        super(props)
        //Ref类型
        this.iptRef = React.createRef<HTMLInputElement>()
    }
    handleClick = () => {
       console.log(this.iptRef)
       this.iptRef.current?.focus()
    }
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>非受控</legend>
                <button onClick={this.handleClick}>点击</button>
                <input type="text" ref={this.iptRef} />
            </fieldset>
        )
    }
}

export default NotControl