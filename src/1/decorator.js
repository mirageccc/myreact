//装饰器语法

//一个包装的过程

//不能装饰函数

//动态属性需要类实例化，开辟内存之后才能访问

//静态成员不需要实例化，和类一起存在的
@IronMan
class Humen {
    name="蔡嘉豪"
}
// console.log(Humen.name) //类访问不到 动态属性

let h = new Humen()

// console.log(h.name)

function IronMan(target) {//修饰函数
    target.name2 = "韩梅梅"
    target.skill = "laser"
}
// IronMan(Humen)

// console.log(Humen.skill)