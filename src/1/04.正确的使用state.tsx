import React from "react"

//声明props
interface p {
    [props: string]: any
}
//声明state

interface s {
    name: string
    age: number
}

class Corront extends React.Component<p, s> {
    constructor(props: p) { //传入props好处就是可以在constructor中获取props
        super(props)
        this.state = {
            name: "李雷",
            age: 30
        }
    }

    changeState() {
        //正确使用组件的状态
        //1.不要直接更新state
        // this.state.name = '韩梅梅'
        //2.状态的更新可能是异步的

        //3.状态的更新会进行合并 如果在一个函数里多次调用setState那么会合并成一次调用来提升性能
    };

    componentDidMount() {
        this.changeState()
    }

    //编写组件结构
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>正确使用组件状态</legend>
                <h1>name:{this.state.name}</h1>
                <h1>age:{this.state.age}</h1>
            </fieldset>
        )
    }
}

export default Corront