import {Component,ReactNode,Fragment} from "react"
import  BoilingVerdict from "./BoilingVerdic"
import TemperatureInput from "./TemperatureInput"

//引入转换函数方法

import convertFn from "./utils/convertFn"

type Istate = {
    temperature: string |number
    scale: string
}

//容器父组件
class Calculator extends Component<{},Istate> {
    constructor(props:any){
        super(props)
        this.state = {//状态提升到了父组件
            temperature:'',
            scale:'c' //这个字段可以用于标记我在哪个输入框
        }
    }
    //处理摄氏度
    handleCelsiusChange = (tem:string)=>{
        console.log(this.state.scale)
        this.setState({
            temperature:tem,
            scale:"c"

        })
    }
    //处理华氏度
    handleFahrenheitChange = (tem:string)=>{
        console.log(this.state.scale)
        console.log(tem)
        this.setState({
            scale:"f"
        })  
    }
    render():ReactNode{
        let {temperature, scale} = this.state;

        //荣耀王者操作

        let celsius = scale === "c" ? temperature:
        convertFn(temperature as string,'toCelsius')

        let fahrenheit = scale === 'f' ? temperature :
        convertFn(temperature as string,'toFahrenheit')

        return (
            <Fragment>
                <TemperatureInput handleTemperatureChange={this.handleCelsiusChange} scale= 'c'  temperature={celsius}/>
                <TemperatureInput handleTemperatureChange={this.handleFahrenheitChange} scale= 'f' temperature={fahrenheit}/>
                <BoilingVerdict temperature={celsius} />
            </ Fragment>
        )
    }
}

export default Calculator