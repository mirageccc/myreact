interface convert { //类型限制
  [c:string]: (f:string | number) => number
}

let convertObj:convert = {//转换函数对象
    toCelsius(fahrenheit) {//转换成摄氏度
        return (fahrenheit as number - 32) * 5 / 9;
    },
    toFahrenheit(celsius) {//转化成华氏度
        return (celsius as number * 9 / 5) + 32;
    }
}
/**
 * 
 * @param temperature 输入的温度
 * @param convert 转换的函数
 * @returns 
 */

type c = (t:string,convert:string) => string

let tryConvert:c

tryConvert = function(temperature,convert){
    const input = parseFloat(temperature);
    if(Number.isNaN(input)){
        return '';
    }
    const output = convertObj[convert](input)
    const rounded = Math.round(output * 1000)/1000;
    return rounded.toString();
}

export default tryConvert;

