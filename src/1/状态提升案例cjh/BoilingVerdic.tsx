//这个组件用于显示水是否绕开了

import React,{FC} from 'react';

type tem = {
    temperature:number | string
}

const BoilingVerdict:FC<tem> = props =>{
    if(props.temperature >= 100){
        return(
            <h1>水烧开</h1>
        )
    }else{
        return(
            <h1>水凉凉了</h1>
        )

    }
}
export default BoilingVerdict;