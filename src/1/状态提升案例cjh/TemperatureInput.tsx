import { Component, ReactNode, SyntheticEvent}from "react"

import scaleObj from "./scale"

type Istate = {
    temperature: number | string
}

//抽离第二个输入框,要考虑输入的名字 是摄氏度或者华氏度 可以通过scaleObj来展示

//还需要一个props

type Iprops = {
    scale:string;
    temperature:string | number
    handleTemperatureChange:(tem:string) => void;
}

class TemperatureInput extends Component<Iprops,Istate>{
    handleChange = (e:any)=>{
        // console.log(e.target.value)
        // this.setState({
        //     temperature: e.target.value
        // })
        //将输入值传给父组件的方法
        this.props.handleTemperatureChange(e.target.value)
    }
    render():ReactNode{
        // console.log(this.props)
        //传递过来的scaleObj中key标识(属性名)
        let {scale} = this.props;
        return (
            <fieldset>
                {/* <legend>{scaleObj[scale]}</legend> */}
                <input type="text" value={this.props.temperature} onChange={this.handleChange}/>
            </fieldset>
        )
        
    }
}

export default TemperatureInput