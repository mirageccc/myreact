import React, { ReactElement } from "react";

interface Istate {
    isLogin: boolean;
}

interface t {
    loginHTML: React.ReactNode
    logoutHTML: React.ReactNode
}

class Toggle extends React.Component<any, Istate> implements t {
    loginHTML: React.ReactNode;
    logoutHTML: React.ReactNode;
    constructor(props: any) {
        super(props)
        this.state = {
            isLogin: false //用于描述登入状态
        }
        this.loginHTML = <h1>欢迎登入,尊贵的钻石💎会员</h1>
        this.logoutHTML = <h1>请点击登入</h1>
    }

    handleChange = () => {
        // 将state中的isLogin取法
        this.setState({
            isLogin: !this.state.isLogin
        })
    }
    render(): React.ReactNode {
        let { isLogin } = this.state;
        //使用元素变量将结构存储
        // let loginHTML: React.ReactNode = <h1>欢迎登入,尊贵的钻石💎会员</h1>
        // let logoutHTML: React.ReactNode = <h1>请点击登入</h1>
        return (
            <fieldset>
                <legend>切换</legend>
                <button onClick={this.handleChange}>{isLogin ? '退出' : '点击登入'}</button>
                {isLogin ? this.loginHTML : this.logoutHTML}
            </fieldset>
        )
    }
}

export default Toggle