import React from "react"
// 1. 创建一个名称扩展为 React.Component 的ES6 类

// 2. 创建一个叫做render()的空方法

// 3. 将函数体移动到 render() 方法中

// 4. 在 render() 方法中，使用 this.props 替换 props

// 5. 删除剩余的空函数声明

// 6. 为当前组件添加一个局部状态

// 7. 将this.props换成this.state

// 8. 给组件添加生命周期

// 9. 编写一个tick的函数,里面设定定时器,并且更新时间

// 10. 在componentDidMount中调用tick函数

// 11. 在componentWillUnmount这个钩子中卸载定时器

// 首先定义一个interface 定义给state

// React.Component 中的泛型 <prop,state>
interface Istate {
    d: Date
}

class Clock extends React.Component<any, Istate> {
    timer: any; // 声明当前实例上的属性
    constructor(props: any) {
        super(props)
        this.state = { //组件的内部状态
            d: new Date()
        }
    }
    //tick函数
    tick() {
        //更新状态
        this.timer = setInterval(() => {
            //调用更新状态的方法
            this.setState({
                d: new Date
            })
        }, 1000)
    }
    //组件挂载之后 和vue里面的mounted是一样的
    componentDidMount() {
        this.tick()
    }
    //组件卸载之后
    componentWillUnmount() { //组件即将卸载
        clearInterval(this.timer)
    }
    render() { // 钩子函数里面会进行diff比较和更新虚拟dom
        // console.log('clock')
        // console.log(this.props)
        // props.d = '123' props只能读 不能改
        return (
            <fieldset>
                <legend>北京时间</legend>
                <h1>时间滴答滴答就过去啦</h1>
                <h1>{this.state.d.toLocaleTimeString()}</h1>
            </fieldset>
        )
    }
}

export default Clock