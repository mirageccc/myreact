import React, { useRef } from "react"

type p = {
    name: string
    children: never[]
    // ref: React.RefObject<HTMLInputElement>
}

type f = {
    show(): void
}

let Modal = React.forwardRef((props:p, ref) => {
    console.log(props)
    console.log(ref)
    let show = () => {console.log(123) }
    return <h1>123 </h1>
}
)

class Test2 extends React.Component {
    ref: React.RefObject<f>
    constructor(props: any) {
        super(props)
        this.ref = React.createRef<f>()
    }

    render() {
        return (
            <div>
                <Modal name="李雷" ref={this.ref}>
                </Modal>
                <button onClick={() => {
                    console.log(this.ref)
                }}>点击</button>
            </div>
        )
    }
}

export default Test2