import React, { FC, forwardRef } from "react";

//函数组件不能使用ref

//react中提供了一个高级API React.forwardRef(函数组件(props,ref)={})

//forwardRef可以用于转发ref

//需求:在父级的组件中声明一个ref,转发到FnCom这个子组件上,然后给input元素进行引用


//子组件
// const FnCom: FC<{}> = (props) => {
//     return (
//         <fieldset>
//             <legend>函数组件</legend>
//             <input type="text" />
//         </fieldset>
//     )
// }

type Iprops = {
    name: string
    l: (q: any) => void
}

// forwardRef<'暴露的方法','定义的props'>



const FnCom = forwardRef<any, Iprops>((props, ref) => {
    console.log(ref) //3
    console.log(props)
    props.l('7777')
    return (
        <fieldset>
            <legend>函数组件</legend>
            <input type="text" ref={ref} />
        </fieldset>
    )
})

//父组件
class ForwardCom extends React.Component<{}> {
    ref: React.RefObject<HTMLInputElement>
    constructor(props: any) {
        super(props)
        this.ref = React.createRef() //1
    }
    handleClick = () => {
        // console.log(123)
        console.log(this.ref)
    }
    log = (q: any) => {
        console.log(q)
    }
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>forward</legend>
                <button onClick={this.handleClick}>点击</button>
                {/* 2 */}
                <FnCom name='李雷' l={this.log} ref={this.ref} />
            </fieldset>
        )
    }
}

export default ForwardCom