//注册事件在类里面 主要是因为this问问题要处理
import React from 'react'

interface e { //接口定义类
    handleChange(a: string, e?: any): void
}

type Istate = { //state的类型
    name: string
}
type Iprops = {//props
    name: string | undefined
}
class EventComp extends React.Component<Iprops> implements e {
    public state: Istate //公开state属性 不然是readonly 不能直接修改
    constructor(props: Iprops) {
        super(props)
        this.state = {
            name: "李雷"
        }
    }

    handleChange(a: string, e?: any): void { //事件处理函数
        //event对象,总是在其它参数的最后面
        console.log(123)
        console.log(this.state)
        this.state.name = '韩梅梅'
        console.log(this.state)
        console.log('a', a)
        console.log('e', e)
    }
    handle2Change(a: any, e: any) {
        console.log(a) //合成事件对象
    }
    render(): React.ReactNode {
        let { name } = this.state;
        return (
            <fieldset>
                <legend>注册事件</legend>
                <h1>{name}</h1>
                <button onClick={this.handleChange.bind(this, 'string')}>点击</button>
                <button onClick={this.handle2Change.bind(this, 123)}>点击2</button>
            </fieldset>
        )
    }
}

export default EventComp