// 构建一个函数组件




//import { FC } from "react" //FunctionComponent
import React, { FC } from "react"

// interface FC<T> {
//     (props: T): React.ReactElement
// }

interface Iprops { //接口
    name?: string | undefined | number
    //索引签名
    [key: string]: number | undefined | string
}

//声明一个元素变量

// let el: React.ReactElement = <h1>react和ts真好学</h1>

let els: string = "<h1>react真好学</h1>"

// dangerouslySetInnerHTML 用于设置字符串类型的html

const RenderHtml: FC<Iprops> = (props) => {
    return (
        <fieldset>
            <legend>渲染HTML的函数组件</legend>

            <div className="container" dangerouslySetInnerHTML={{ __html: els }}></div>
        </fieldset>
    )
}

export default RenderHtml