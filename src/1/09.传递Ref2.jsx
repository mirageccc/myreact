import React, { FC } from "react";

//函数组件不能使用ref

//react中提供了一个高级API React.forwardRef(函数组件(props,ref)={})

//forwardRef可以用于转发ref

//子组件
const FnCom = (props) => {
    return (
        <fieldset>
            <legend>函数组件</legend>
            <input type="text" />
        </fieldset >
    )
}

//父组件
class ForwardCom extends React.Component {
    constructor(props) {
        super(props)
        this.ref = React.createRef()
    }
    handleClick = () => {
        console.log(123)
    }
    render() {
        return (
            <fieldset>
                <legend>forward</legend>
                <button onClick={this.handleClick}>点击</button>
                <FnCom />
            </fieldset>
        )
    }
}

export default ForwardCom