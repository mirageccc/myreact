import React from "react"
import Other from "./08.其它组件"


//点击按钮获得焦点

//非受控的方式通过ref来实现

//ref可作用在组件和元素上 获取真实的dom元素

//ref作用在组件上可以获取组件实例的引用

class Not2 extends React.Component {
    ref: React.RefObject<HTMLInputElement>
    ref2: any
    constructor(props: any) {
        super(props)
        //创建ref
        this.ref = React.createRef<HTMLInputElement>()
        this.ref2 = React.createRef()
    }
    //点击的方法
    handleClick = () => {
        console.log(this.ref)
        //获取焦点
        this.ref.current?.focus()
        //输出子组件的引用
        console.log(this.ref2)
        this.ref2.current.state.name = '韩梅梅'
    }
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>非受控</legend>
                <button onClick={this.handleClick}>点击</button>
                <input type="text" ref={this.ref} />
                <Other ref={this.ref2} />
            </fieldset>
        )
    }
}

export default Not2