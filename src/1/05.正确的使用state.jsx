import React from "react"


class Corront extends React.Component {
    constructor(props) { //传入props好处就是可以在constructor中获取props
        super(props)
        this.state = {
            name: "李雷",
            age: 30
        }
    }

    changeState() {
        //正确使用组件的状态
        //1.不要直接更新state
        // this.state.name = '韩梅梅'
        // console.log(this.state)
        //调用this.setState来变更状态 调用setState会触发render的调用
        // setTimeout(() => {
        //     this.setState({})
        // }, 2000)

        // this.setState({
        //     name: "韩梅梅"
        // })

        //2.状态的更新可能是异步的

        this.setState({
            age: 50
        })

        //console.log(this.state.age) //30 ❌
        // setTimeout(()=>{ //相当于是nextTick
        //     console.log(this.state.age) //50 ✅
        // })

        //react官方给的方法
        //preState上一个状态,preProps上一个props
        // this.setState((preState, preProps) => {
        //     console.log(preState, preProps)
        //     return { //返回去的结果会更新视图
        //         age: 80
        //     }
        // })

        //3.状态的更新会进行合并 如果在一个函数里多次调用setState那么会合并成一次调用来提升性能
        this.setState({
            name: "韩梅梅"
        })

        this.setState({
            age: 12
        })
        this.setState({
            age: 40
        })
    };

    componentDidMount() {
        this.changeState()
    }

    //编写组件结构
    render() { //进行diff算法 更新虚拟dom
        console.log('render触发了')
        return (
            <fieldset>
                <legend>正确使用组件状态</legend>
                <h1>name:{this.state.name}</h1>
                <h1>age:{this.state.age}</h1>
            </fieldset>
        )
    }
}

export default Corront