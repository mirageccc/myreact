import { Component, ReactNode } from "react"

type p = {
    nickname: string
}

class Child extends Component<p> {
    constructor(props: any) {
        super(props)
    }
    //即将接收props的时候
    componentWillReceiveProps(newProps: any) {
        // 组件初始化时不调用，组件接受新的props时调用。
        console.log('newProps', newProps)
    }
    // shouldComponentUpdate() {
    // react性能优化非常重要的一环。组件接受新的state或者props时调用，我们可以设置在此对比前后两个props和state是否相同，如果相同则返回false阻止更新，因为相同的属性状态一定会生成相同的Vdom树，这样就不需要创造新的Vdom树和旧的Vdom树进行diff算法对比，节省大量性能，尤其是在Vdom结构复杂的时候
    //  }
    componentWillUpdate(nextProps: any, nextState: any) {
        // 组件初始化时不调用，只有在组件将要更新时才调用，此时不可以修改state
        console.log('子组件willUpdate下一个', nextState)
    }
    componentDidUpdate(prevProps: any, prevState: any) {
        // 组件初始化时不调用，组件更新完成后调用，此时可以获取dom节点。
        console.log('子组件DidUpdate上一个', prevState)
    }
    componentWillUnmount() {
        //卸载组件
        console.log('组件卸载了')
    }
    componentWillMount() {
        console.log('子组件的willMount')
    }
    componentDidMount() {
        console.log('子组件的DidMount')
    }

    render(): ReactNode {
        console.log('子组件的render')
        return (
            <fieldset>
                <legend>child</legend>
                <h1>父组件的props:{this.props.nickname}</h1>
            </fieldset>
        )
    }
}

interface Istate {
    name: string
}

class LifeCycle extends Component<{}, Istate> {
    constructor(props: any) {
        super(props)
        this.state = {
            name: "李雷"
        }
    }
    componentWillMount() {
        //组件初始化时只调用，以后组件更新不调用，整个生命周期只调用一次，此时可以修改state。异步请求不要在此钩子中发起,容易造成无限更新
        console.log('父组件:willMount')
    }
    handleClick = () => {
        this.setState({ name: "韩梅梅" })
    }
    render(): ReactNode {
        // react最重要的步骤，创建虚拟dom，进行diff算法，更新Vdom树都在此进行。此时就不能更改state了。
        console.log("父组件的:render")
        return (
            <fieldset>
                <legend>生命周期</legend>
                <h1>{this.state.name}</h1>
                <button onClick={this.handleClick}>点击改变</button>
                <Child nickname={this.state.name} />
            </fieldset>
        )
    }
    componentDidMount() {
        // 组件渲染之后调用，只调用一次。
        //这里可以操作dom了
        console.log("父组件 didMount")
    }

    componentWillUpdate(nextProps: any, nextState: any) {
        console.log('父组件:willUpdate')
        console.log('下一个state', nextState)
    }
    componentDidUpdate(prevProps: any, prevState: any) {
        console.log('父组件:didUpdate')
        console.log('上一个state', prevState)
    }
    //性能优化的钩子函数
    shouldComponentUpdate(nextProps: any, nextState: any) {
        //返回true 继续渲染 返回false 逻辑停止
        // console.log('父组件中最新的状态:', nextState, nextProps)
        // 对比新旧的state和对比新旧的props
        console.log("=====旧的", this.state)

        if (this.state.name === nextState.name) {
            return false
        } else {
            return true
        }

        return true
    }
}

export default LifeCycle