import React from "react"
interface Istate {
    name: string
}
class Other extends React.Component<any, Istate> {
    constructor(props: any) {
        super(props)
        this.state = {
            name: "李雷"
        }
    }
    sayHi() {
        console.log('hello my name is hmm how are you')
    }
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>其它组件</legend>
                <h1>其它组件</h1>
                <h2>name:{this.state.name}</h2>
            </fieldset>
        )
    }
}

export default Other