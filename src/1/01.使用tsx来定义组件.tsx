//组件类型

//1.无状态组件 =》 react 16.8以前将函数组件称之为无状态组件

//2.有状态组件

//3.虚拟组件 Fragment

//4.原生组件系列 ReactDOM

//5.懒加载组件


//声明一个无状态组件
//组件名字的首字符要大写

interface p {
    name:string
}
//jsx的表达式和vue里面的插值表达式几乎一样
//1.可以使用三元表达式
//2.可以调用一些方法
//3.不要写 if else
//4.能做数学运算

//声明一个元素变量

//react元素生命出来就像动画里的一帧，是不可变的,除非用一个新的值覆盖掉它
let el:  JSX.Element |number = <h1>123</h1>
console.log(el);
el=123
console.log(el);

function Test({name}:p){
    return  (<fieldset>
        <legend>一个组件</legend>
        <h1>第一个组件</h1>
        {/* <h2>{name.split("").reverse().join("")}</h2> */}
        <h2></h2>
    </fieldset>)
}

export default Test 