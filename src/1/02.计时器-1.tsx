function Clock (props:any){

    console.log(props)
    //props.d = '123' props只能读 不能改
    
    return (
        <fieldset>
            <legend>北京事件</legend>
            <h1>时间滴答滴答就过去啦</h1>
            {/* <h1>{props.d.toLocaleTimeString()}</h1> */}
        </fieldset>   
    )
}

Clock.defaultProps = {//定义默认props
  d:null,
  name:'李雷'
}

//总结:react props是只读的 必须像纯函数一样去使用它,数据流都是遵循自顶向下流动

export default Clock