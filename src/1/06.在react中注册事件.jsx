import React from 'react';

class EventCom extends React.Component {
    //声明state
    state = {
        name: '李雷'
    }
    //定义事件处理函数 使用箭头函数定义
    handle = (name, e) => {
        // console.log(this)
        console.log(e) //默认传入
        console.log(this.state)
        console.log(name)
    }
    render() {
        return (
            <fieldset>
                <legend>注册事件</legend>
                {/* 第一种方式 */}
                {/* <button onClick={() => {
                    console.log('事件')

                }}>点击</button> */}
                <button onClick={this.handle}>点击2</button>
                {/* 怎么传参? */}

                <button onClick={(e) => {
                    //e会被事件默认导入
                    this.handle('李雷', e)
                }}>点击3</button>
            </fieldset>
        )
    }
}

export default EventCom