import React from 'react';


type Istate = {
    val: string
    sel: string
    check: boolean
}

//受控组件的概念:值由react控制的表单输入元素就称之为受控组件

class Control extends React.Component<any, Istate> {
    constructor(props: any) {
        super(props)
        this.state = {
            val: '',
            sel: '',
            check: false
        }
    }
    //处理普通输入
    handleChange = (e: any) => {
        // console.log(e.target.value)
        this.setState({ val: e.target.value })
    }
    //处理下拉选择
    handleChange2 = (e: any) => {
        console.log(e.target.value)
        this.setState({ sel: e.target.value })
    }
    render(): React.ReactNode {
        return (
            <fieldset>
                <legend>受控組件</legend>
                <input type="text" value={this.state.val} onChange={this.handleChange} />
                <select value={this.state.sel} onChange={this.handleChange2}>
                    <option value="1">苹果</option>
                    <option value="2">香蕉皮</option>
                    <option value="3">橘子</option>
                </select>
                {/* 受控 checkbox这类表单元素的时候 应该受控checked属性而不是value */}
                <input type="checkbox" checked={this.state.check} onChange={
                    (e) => {
                        console.log(e.target.checked)
                        this.setState({
                            check: e.target.checked
                        })
                    }
                } />

            </fieldset>
        )
    }
}

export default Control