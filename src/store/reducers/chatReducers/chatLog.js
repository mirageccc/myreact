let initialState = ['你好', 'how are you', 'I am fine thank you', 'and you']


let chatLogReducer = (state = initialState, action) => {
    //判断action.type类型
    let { type, payload } = action;

    switch (type) {
        case "ADD_CHAT":
            return [payload, ...state]
        default:
            return [...state]
    }

}

export default chatLogReducer