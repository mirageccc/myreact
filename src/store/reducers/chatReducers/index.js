//当你感觉返回一个新的state很难操作的时候,这时候就要拆分reducer了
// let defaultState = {
//     chatLog: ['你好', 'how are you', 'I am fine thank you', 'and you'],
//     name: "卢本伟",
//     isVip: false,
//     count: 30
// }

// //redux 一定要返回一个新的状态
// const chat = (state = defaultState, action) => {
//     let { type, payload } = action;
//     switch (type) {
//         case 'ADD_CHAT':
//             let chatLog = [payload, ...state.chatLog];
//             return Object.assign({}, state, {
//                 chatLog
//             })
//         case 'CHANGE_VIP':
//             return Object.assign({}, state, {
//                 isVip: true
//             })

//         default:
//             return { ...state }
//     }

// }

// export default chat

import { combineReducers } from "redux" //合并reducers

//引入拆分的reudcer
import chatLog from "./chatLog"
import isVip from "./isVip"
import count from "./count"
import name from "./name"

const chatReducer = combineReducers({
    chatLog,
    isVip,
    count,
    name
})


export default chatReducer