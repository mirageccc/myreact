// action是一个对象,用于告诉redux 要修改state了

//这个对象中有一个属性叫做type 是必须的 其它的属性可以自由传递

// export const INCREASE_COUNT = {
//     type: 'INCREASE_COUNT',//必须的
//     payload: 10
// }

// export const DECREASE_COUNT = {
//     type: 'DECREASE_COUNT',//
//     payload: 5
// }

export const DECREASE_COUNT = 'DECREASE_COUNT' //减少数量


export const INCREASE_COUNT = 'INCREASE_COUNT' //增加数量