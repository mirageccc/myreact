//actionCreator就是一个函数,函数返回一个action对象


const actionCreator = (type, payload) => ({ type, payload })


export default actionCreator