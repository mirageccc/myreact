//store就是仓库的概念
//是一个容器,一个应用里面只能有一个store
// import counterReducer from "./reducers/counter"
import chatReducer from "./reducers/chatReducers"


import { configureStore } from "@reduxjs/toolkit"

export default configureStore(
    {
        reducer: chatReducer
    }
)

// import { createStore } from "redux"

// export default createStore(counterReducer)